import 'package:flutter/material.dart';
import 'package:pay_track/providers/transaction.dart';
import 'package:pay_track/providers/transactions.dart';
import 'package:pay_track/screens/edit_transaction_screen.dart';
import 'package:pay_track/screens/list_transaction_screen.dart';
import 'package:pay_track/screens/overview_screen.dart';
import 'package:provider/provider.dart';

Future main() async {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => Transactions()),
      ],
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: MyHomePage(),
          routes: {
            OverviewScreen.routeName: (ctx) => OverviewScreen(),
            EditTransactionScreen.routeName: (ctx) => EditTransactionScreen(),
            ListTransactionScreen.routeName: (ctx) => ListTransactionScreen(),
          }),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController _myPage = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _myPage,
        onPageChanged: (int) {
          print('Page Changes to index $int');
        },
        children: <Widget>[
          OverviewScreen(),
          ListTransactionScreen(),
        ],
        physics:
            NeverScrollableScrollPhysics(), // Comment this if you need to use Swipe.
      ),
      floatingActionButton: Container(
        height: 65.0,
        width: 65.0,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {
              Navigator.of(context).pushNamed(EditTransactionScreen.routeName);
            },
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            // elevation: 5.0,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Theme.of(context).primaryColor,
        shape: CircularNotchedRectangle(),
        notchMargin: 8.0,
        child: Container(
          height: 60,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FittedBox(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.home),
                        visualDensity: VisualDensity.compact,
                        iconSize: 30,
                        padding: EdgeInsets.all(1),
                        //padding: EdgeInsets.only(left: 28.0),
                        onPressed: () {
                          setState(() {
                            _myPage.jumpToPage(0);
                          });
                        }),
                    Text(
                      "overview",
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
              SizedBox(),
              FittedBox(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.list),
                        visualDensity: VisualDensity.compact,
                        iconSize: 30,
                        padding: EdgeInsets.all(1),
                        //padding: EdgeInsets.only(left: 28.0),
                        onPressed: () {
                          setState(() {
                            _myPage.jumpToPage(1);
                          });
                        }),
                    Text(
                      "details",
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
