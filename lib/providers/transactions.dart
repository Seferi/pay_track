import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pay_track/helpers/db_helper.dart';
import '../providers/transaction.dart';

class Transactions with ChangeNotifier {
  Map<String, Transaction> _items = {};

  Map<String, Transaction> get items {
    return {..._items};
  }

  List<String> _allTags = [];

  List<String> get allTags {
    return _allTags;
  }

  Future<void> fetchAndSetTransactionsAndTags() async {
    final dataList = await DBHelper.getData("user_transactions");
    List<Transaction> transList = dataList
        .map((trans) =>
        Transaction(
          id: trans["id"],
          title: trans["title"],
          desc: trans["desc"],
          amount: double.parse(trans["amount"]),
          tags: Transaction.getStringAsTagsList(trans["tags"]),
          type: Transaction.getStringAsType(trans["type"]),
          frequency: Transaction.getStringAsFrequency(trans["frequency"]),
        ))
        .toList();
    Map<String, Transaction> transMap = {};
    transList.forEach((trans) {
      transMap.putIfAbsent(trans.id, () => trans);
    });
    _items = transMap;
    _allTags = [];
    transList.forEach((trans) {
      trans.tags.forEach((tag) {
        if (!_allTags.contains(tag)) {
          _allTags.add(tag);
        }
      });
    });
    notifyListeners();
  }

  Map<String, Transaction> transactionsPerTag(List<String> tags) {
    Map<String, Transaction> result = {};
    tags.forEach((tag) {
      items.values.forEach((transaction) {
        if (transaction.tags.contains(tag)) {
          result.putIfAbsent(transaction.id, () => transaction);
        }
      });
    });
    return result;
  }

  int incomeTransCount() {
    var count = 0;
    items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Income) {
        count++;
      }
    });
    return count;
  }

  int paymentTransCount() {
    var count = 0;
    items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Payment) {
        count++;
      }
    });
    return count;
  }

  double getTotalPaymentPercentage() {
    var result = (totalYearlyPayments() / totalYearlyIncome()) * 100;
    return result;
  }

  Transaction findById(String id) {
    return _items.values.firstWhere((transaction) => transaction.id == id);
  }

  double totalMonthlyIncome() {
    var income = 0.0;
    _items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Income &&
          transaction.frequency == TransactionFrequency.Monthly) {
        income += transaction.amount;
      }
    });
    return income;
  }

  double totalYearlyIncome() {
    var income = 0.0;
    _items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Income &&
          transaction.frequency == TransactionFrequency.Yearly) {
        income += transaction.amount;
      }
      if (transaction.type == TransactionType.Income &&
          transaction.frequency == TransactionFrequency.Monthly) {
        income += transaction.amount * 12;
      }
    });
    return income;
  }

  double totalMonthlyPayments() {
    var spent = 0.0;
    _items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Payment &&
          transaction.frequency == TransactionFrequency.Monthly) {
        spent += transaction.amount;
      }
    });
    return spent;
  }

  double totalYearlyPayments() {
    var spent = 0.0;
    _items.values.forEach((transaction) {
      if (transaction.type == TransactionType.Payment &&
          transaction.frequency == TransactionFrequency.Yearly) {
        spent += transaction.amount;
      }
      if (transaction.type == TransactionType.Payment &&
          transaction.frequency == TransactionFrequency.Monthly) {
        spent += transaction.amount * 12;
      }
    });
    return spent;
  }

  void addTransaction(Transaction transaction) {
    _items.putIfAbsent(transaction.id, () => transaction);
    transaction.tags.forEach((tag) {
      if (!_allTags.contains(tag)) {
        _allTags.add(tag);
      }
    });
    DBHelper.insert("user_transactions", {
      "id": transaction.id,
      "title": transaction.title,
      "desc": transaction.desc,
      "amount": transaction.amount.toString(),
      "tags": transaction.tags.toString(),
      "type": transaction.type.toString(),
      "frequency": transaction.frequency.toString(),
    });
    notifyListeners();
  }

  void deleteTransaction(String id, String table) {
    _items.remove(id);
    DBHelper.delete(id, table);
    notifyListeners();
  }
}
